var mongoose = require('mongoose');

var postSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    userData: {
        facebookPicUrl: String,
        username: String,
        fullname: String
    },
    songData: {
        name: String,
        artist: String,
        album: String,
        artworkUrl: String,
        deezerUrl: String,
        appleMusicUrl: String,
        spotifyUrl: String
    },
    body: String,
    hashtags: [String],
    tagged: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    created: {
        type: Date,
        default: Date.now
    },
    numLikes: {
        type: Number,
        default: 0
    },
    numShares: {
        type: Number,
        default: 0
    },
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ],
    location: {
        lat: Number,
        long: Number,
        address: String
    },
    likedByMe: Boolean,
    isRepostByMe: Boolean,
    isRepostOf: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post'
    },
    repostUserData: {
        username: String,
        fullname: String
    }
});

postSchema.methods.like = function(cb) {
    this.numLikes += 1;
    this.save(cb);
}

postSchema.methods.unlike = function(cb) {
    if (this.numLikes >0) this.numLikes -= 1;
    this.save(cb);
}

postSchema.methods.repost = function(cb) {
    this.numShares += 1;
    this.save(cb);
}

postSchema.methods.unshare = function(cb) {
    if (this.numShares >0) this.numShares -= 1;
    this.save(cb);
}

// share/unshare ?

mongoose.model('Post', postSchema, 'posts');
