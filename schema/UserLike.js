var mongoose = require('mongoose');

var userLikeSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    postId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post'
    }
});

mongoose.model('UserLike', userLikeSchema, 'userlike');
