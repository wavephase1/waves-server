var mongoose = require('mongoose');

var notificationSchema = new mongoose.Schema({
    body: String,
    created: {
        type: Date,
        default: Date.now
    },
    payload: {
        notifType: Number,
        id: String
    }
});

mongoose.model('Notification', notificationSchema, 'notifications');
