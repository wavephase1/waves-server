var mongoose = require('mongoose');

var followRequestSchema = new mongoose.Schema({
    followeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    followerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

mongoose.model('FollowRequest',followRequestSchema, 'followRequests');
