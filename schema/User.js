var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    username: {
        type: String,
        lowercase: true,
        unique: true,
        sparse: true
    },
    fullname: String,
    facebookId: {
        type: String,
        unique: true,
        sparse: true
    },
    emailId: {
        type: String,
        unique: true,
        sparse: true
    },
    deviceToken: String,
    bio: String,
    isPrivate: {
        type: Boolean,
        default: false
    },
    facebookPicUrl: {
        type: String,
        default: null
    },
    songData: {
        name: String,
        artist: String,
        album: String,
        artworkUrl: String,
        deezerUrl: String,
        appleMusicUrl: String,
        spotifyUrl: String
    },
    numFollowers: Number,
    numFollowing: Number,
    allNotifications: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Notification'
        }
    ],
    tagNotifications: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Notification'
        }
    ],
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('User', userSchema, 'users');
