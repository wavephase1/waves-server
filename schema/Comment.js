var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    postId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post'
    },
    userData: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    body: String,
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('Comment', commentSchema, 'comments');
