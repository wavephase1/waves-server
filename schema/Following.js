var mongoose = require('mongoose');

var followingSchema = new mongoose.Schema({
    followeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    followerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Following', followingSchema, 'following');
