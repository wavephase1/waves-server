"use strict";
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const apn = require('apn');

const apnProvider = new apn.Provider({
    cert: "./cert/cert.pem",
    key: "./cert/key.pem",
    production: true
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

require('./schema/User.js');
require('./schema/Post.js');
require('./schema/Comment.js');
require('./schema/Following.js');
require('./schema/FollowRequest.js');
require('./schema/UserLike.js');
require('./schema/Notification.js');
const User = mongoose.model('User');
const Post = mongoose.model('Post');
const Comment = mongoose.model('Comment');
const Following = mongoose.model('Following');
const FollowRequest = mongoose.model('FollowRequest');
const Notification = mongoose.model('Notification');
const UserLike = mongoose.model('UserLike');

mongoose.connect('mongodb://localhost/waves');

let serverError = { 
    status: 500, 
    data: null,
    message: 'There was an error. Please try again later.' 
};


/*
 * Create new user or login if user exists
 */
app.post('/api/login', function(req, res, next) {
    if (!req.body.facebookId || !req.body.emailId) {
        return res.json({
            status: 400,
            data: null,
            message: 'Email and facebook id required.'
        });
    }

    User.findOne({ facebookId: req.body['facebookId'], emailId: req.body['emailId'] })
        .then(user => {
            if (user) {
                // Login user
                user['isExistingUser'] = true;
                user['deviceToken'] = req.body['deviceToken'];
                console.log(req.body.deviceToken);
                return user.save();
            } else {
                // Register user
                let user = new User();
                user['facebookId'] = req.body['facebookId'];
                user['facebookPicUrl'] = req.body['facebookPicUrl'];
                user['username'] = req.body['username'];
                user['fullname'] = req.body['fullname'];
                user['emailId'] = req.body['emailId'];
                user['deviceToken'] = req.body['deviceToken'];
                return user.save();
            }
        })
        .then(user => {
            let username = user['username'];
            let message = '';
            if (user['isExistingUser']) {
                message = 'Welcome back' + ((username) ? ', ' + username : '') + '!';
            } else {
                message = 'Registration successful!';
            }
            return res.json({
                status: 200,
                data: user,
                message: message
            });
        })
        .catch(err => {
            console.log(err);
            if ((err.name === 'BulkWriteError' || err.name === 'MongoError') && err.code === 11000) {
                return res.json({
                    status: 500,
                    data: null,
                    message: 'A user with that either that facebook id or email id already exists.'
                });
            }
            return res.json(serverError);
        });
});

/*
 * Clear device token
 */
app.post('/api/logout', function(req, res, next) {
    if (!req.body.facebookId || !req.body.emailId) {
        return res.json({
            status: 400,
            data: null,
            message: 'Email and facebook id required.'
        });
    }

    User.findOne({ facebookId: req.body['facebookId'], emailId: req.body['emailId'] })
        .then(user => {
            user['deviceToken'] = null;
            return user.save();
        })
        .then(user => {
            return res.json({
                status: 200,
                data: user,
                message: 'Successfully logged out'
            });
        })
        .catch(err => {
            console.log(err);
            return res.json(serverError);
        });
});

/*
 * Update properties of a user
 */
app.post('/api/users/:userId', function(req, res, next) {
    const validParams = [
        'username', 'bio', 'isPrivate',
        'fullname', 'facebookPicUrl', 'songData'
    ];

    // cycle through params, err if mismatch
    for (let key in req.body) {
        if (req.body.hasOwnProperty(key)) {
            if (validParams.indexOf(key) === -1) {
                return res.json({
                    status: 400,
                    data: null,
                    message: key + ' is not a valid user property.'
                });
            }
        }
    }
    // get user, set params of user and save and return
    User.findById(req.params.userId)
        .then(user => {
            if (!user) {
                return null;
            }

            for (let key in req.body) {
                if (req.body.hasOwnProperty(key)) {
                    let value = req.body[key];
                    user[key] = value;
                }
            }

            return user.save();
        })
        .then(user => {
            if (user) {
                return res.json({
                    status: 200,
                    data: user,
                    message: 'Profile settings updated successfully.'
                });
            } else {
                return res.json({
                    status: 500,
                    data: null,
                    message: 'User does not exist.' 
                });
            }
        })
        .catch((err) => {
            if (err.name === 'MongoError' && err.code === 11000) {
                return res.json({
                    status: 500,
                    data: null,
                    message: 'That username is already taken!'
                });
            }
            return res.json(serverError);
        });

});

/*
 * Get a list of all users
 */
app.get('/api/users', function(req, res, next) {
    User.find({}, function(err, users) {
        if (err) {
            return res.json({
                status: 500,
                data: null,
                message: 'There was an error. Please try again later.' 
            });
        }
        return res.json({
            status: 200,
            data: users,
            message: 'Success'
        });
    });
});
/*
 * Return trending hashtags.
 */
app.get('/api/discover', function(req, res, next) {

    Post.aggregate([
             {$match: {}}, 
             {$project: {hashtags: 1}},
             {$unwind: '$hashtags'},
             {$group: {_id: { hashtag: '$hashtags'}, count: {$sum: 1}}},
             {$sort: { count: -1 }}
        ])
        .then(tags => {
            const numTags = 20;
            let slicedTags  = tags.slice(0, numTags);
            let returnTags = [];
            for (let tag of slicedTags) {
                returnTags.push({ tag: tag['_id']['hashtag'], count: tag['count']});
            }
            return res.json({
                status: 200,
                data: returnTags,
                message: 'Most popular hashtags.'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
    
});

/*
 * Most liked and most recent posts with hashtag
 */
app.get('/api/discover/:hashtag', function(req, res, next) {
    let hashtag = req.params.hashtag;
    if (hashtag.substring(0,1) !== '#') {
        hashtag = '#' + hashtag;
    }

    let data = {};

    // Get most liked posts
    Post.find({ hashtags: {$in: [ hashtag ]}})
        .sort({ numLikes: -1 })
        .then(posts => {
            data.mostLiked = posts;

    // Get most recent posts
            return Post.find({ hashtags: {$in: [ hashtag ]}}).sort({ created: -1 });
        })
        .then(posts => {
            data.mostRecent = posts;

            return res.json({
                status: 200,
                data: data,
                message: 'Posts with hashtag ' + req.params.hashtag
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
});

/*
 * Return most recent notifications
 */ 
app.get('/api/notifications/:userId', function(req, res, next) {

    let thisUser = {};
    let response = {
    };
    User.findById(req.params.userId)
        .then(user => {
            thisUser = user;
            return Notification.find({ _id: { $in: user.allNotifications }});
        })
        .then(notifs => {
            response.allNotifications = notifs;
            return Notification.find({ _id: { $in: thisUser.tagNotifications }});
        })
        .then(notifs => {
            response.tagNotifications = notifs;
            return res.json( {
                status: 200,
                data: response,
                message: 'Success.'
            });
        })
        .catch(err => {
        console.log(err);
            return res.json(serverError);
        });

});

/*
 * Return profile data for user. User data and post history
 */
app.post('/api/profile/:userId', function(req, res, next) {
    if (!req.body.userId) {
        return res.json({
            status: 400,
            data: null,
            message: 'ID of logged in user must be defined'
        });
    }

    let returnData = { };
    let userId = req.params.userId;
    let viewerId = req.body.userId;
    let viewerUsername = '';
    User.findById(userId)
        .then(user => {
            returnData['user'] = user;
            return User.findById(viewerId);
        })
        .then(user => {
            viewerUsername = user.username;

        /*
         * set following status. Is following, not following, or requested
         */
            return FollowRequest.findOne({
                followerId: viewerId,
                followeeId: userId
            });
        })
        .then(following => {
            if (following) {
                return 'Requested';
            } else {
                return Following.findOne({
                    followerId: viewerId,
                    followeeId: userId
                });
            }
        })
        .then(following => {
            if (following === 'Requested') {
                returnData['isUserFollowing'] = 'Requested';
            } else {
                returnData['isUserFollowing'] = (following) ? 'Following' : 'Not following';
            }

        /*
         * Follower/following stats
         */
            return Following.count({ followeeId: userId });
        })
        .then(count => {
            returnData['user']['numFollowers'] = count;
            return Following.count({ followerId: userId });
        })
        .then(count => {
            returnData['user']['numFollowing'] = count;

        /*
         * Get the posts this user has created and liked
         */
            return Post.find({ userId: userId })
                       .sort('-created');
        })
        .then(posts => {
            returnData['posts'] = posts;
            return UserLike.find({ userId: userId })
                       .sort('-created');
        })
        .then(userLikes => {
            let likedPostIds = [];
            for (let entry of userLikes) {
                likedPostIds.push(entry['postId']);
            }
            return Post.find({ _id: { $in: likedPostIds } });
        })
        .then(posts => {
            returnData['likedPosts'] = posts;

        /* 
         * Mark posts that are liked by the viewer/logged in user
         */
            return UserLike.find({ userId: viewerId });
        })
        .then(userLikes => {
            let likedPostIds = {};
            for (let entry of userLikes) {
                likedPostIds[entry.postId] = true;
            }

            returnData.posts.map(post => {
                post.likedByMe = (likedPostIds[post._id]) ? true : false;
                return post;
            });
            returnData.likedPosts.map(post => {
                post.likedByMe = (likedPostIds[post._id]) ? true : false;
                return post;
            });
            return returnData;
        })
        .then(data => {
            returnData.posts.map(post => {
                post.isRepostByMe = (post.repostUserData.username === viewerUsername) ? true : false;
                return post;
            });
            returnData.likedPosts.map(post => {
                post.isRepostByMe = (post.repostUserData.username === viewerUsername) ? true : false;
                return post;
            });
            return res.json({
                status: 200,
                data: returnData,
                message: 'Successfully retrieved profile.'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });

});

/*
 * Create new post/"wave"
 */
app.post('/api/posts', function(req, res, next) {
    // TODO post auth
    const requiredParams = [
        {key: 'userId', readable: 'User'},
        {key: 'songData', readable: 'Song'},
        {key: 'body', readable: 'Caption'},
        {key: 'tagged', readable: 'Tagged Users'},
        {key: 'location', readable: 'Location '}
    ];

    let missingParams = getMissingParams(req.body, requiredParams);
    if (missingParams.length > 0) {
        let missingParamStr = missingParams.join(', ');
        return res.json({
            status: 400,
            data: null,
            message: missingParamStr + ' is/are required.'
        });
    } else {
        let post = new Post();
        for (let param of requiredParams) {
            let key = param['key']
            post[key] = req.body[key];
        }
        // Strip hashtags from post body
        let hashtags = post['body'].match(/#\S+/g);
        post['hashtags'] = hashtags;

        // Send notifications to tagged users
        for (let taggedUser of post.tagged) {
            sendNotification(req.body['userId'],
                             taggedUser,
                             ' tagged you in a post.',
                             { notifType: 2, isTag: true, id: post._id });
        }

        User.findById(req.body.userId)
            .then(user => {
                post['userData'] = {
                    'username': user['username'],
                    'fullname': user['fullname'],
                    'facebookPicUrl': user['facebookPicUrl']
                };
                return post.save();
            })
            .then(post => {
                return res.json({
                    status: 200,
                    data: post,
                    message: 'Successfully created new Wave!'
                });
            })
            .catch(err => {
                console.log(err);
                return res.json(serverError);
            });
    }
});

/*
 * Get post by id
 */
app.get('/api/posts/:postId', function(req, res, next) {
    Post.findById(req.params.postId)
        .then(post => {
            return res.json({
                status: 200,
                data: post,
                message: 'Post from ' + post.created
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
});

/*
 * Get comments on post by id
 */
app.get('/api/posts/:postId/comments', function(req, res, next) {
    let thisComments = [];
    Post.findById(req.params.postId)
        .then(post => {
            return Comment.find({_id: { $in: post.comments }});
        })
        // Get data of users in comments
        .then(comments => {
            thisComments = comments;
            let commentUserIds = [];
            for (let comment of comments) {
                commentUserIds.push(comment.userId);
            }
            return User.find({ _id: { $in: commentUserIds }});
        })
        // Match user data to comment
        .then(users => {
            let hashedUsers = {};
            for (let user of users) {
                hashedUsers[user._id] = user;
            }
            for (let i = 0; i < thisComments.length; i++) {
                let thisUserId = thisComments[i].userId;
                thisComments[i].userData = hashedUsers[thisUserId];
            };
            return res.json({
                status: 200,
                data: thisComments,
                message: 'Successfully retrieved comments'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
});

app.post('/api/posts/:postId/like', function(req, res, next) {
    if (!req.body.userId) {
        return res.json({
            status: 400,
            data: null,
            message: 'ID of user liking the post must be defined'
        });
    }
    Post.findById(req.params.postId)
        // Like post
        .then(post => {
            if (!post) { return res.json(serverError); }
            sendNotification(req.body.userId, post.userId, ' liked your wave.', { notifType: 2, id: post._id });
            return post.like();
        })
        // Save in user's liked posts
        .then(post => {
            let userLike = new UserLike();
            userLike.userId = req.body.userId;
            userLike.postId = req.params.postId;
            return userLike.save();
        })
        .then(userLike => {
            return res.json({
                status: 200,
                data: null,
                message: 'Successfully liked post.'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
});

app.post('/api/posts/:postId/unlike', function(req, res, next) {
    if (!req.body.userId) {
        return res.json({
            status: 400,
            data: null,
            message: 'ID of user liking the post must be defined'
        });
    }
    Post.findById(req.params.postId)
        // Decrement like count
        .then(post => {
            if (!post) { return res.json(serverError); }
            return post.unlike();
        })
        // Delete from userlike collection
        .then(post => {
            return UserLike.remove({ userId: req.body.userId, postId: req.params.postId });
        })
        .then(userLike => {
            return res.json({
                status: 200,
                data: null,
                message: 'Successfully unliked post.'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
});

app.post('/api/posts/:postId/share', function(req, res, next) {
    if (!req.body.userId) {
        return res.json({
            status: 400,
            data: null,
            message: 'ID of user sharing the post must be defined'
        });
    }

    let repostUser = null;
    User.findById(req.body.userId)
        .then(user => {
            repostUser = user;
            return Post.findById(req.params.postId);
        })
        .then(post => {
            // if this post is repost, share the original post
            if (post.isRepostOf) {
                return Post.findById(post.isRepostOf);
            } else {
                return post;
            }
        })
        .then(post => {
            post.numShares += 1;
            return post.save();
        })
        .then(post => {
            console.log(post);
            let newPost = new Post(post);
            newPost._id = mongoose.Types.ObjectId();
            newPost.userId = repostUser._id;
            newPost.isNew = true;
            newPost.isRepostOf = post._id;
            newPost.created = Date.now();
            newPost.repostUserData = {
                username: repostUser.username,
                fullname: repostUser.fullname
            };
            sendNotification(req.body.userId, newPost._id, ' shared your wave.', { notifType: 2, id: newPost._id });
            return newPost.save();
        })
        .then(newPost => {
            return res.json({
                status: 200,
                data: newPost,
                message: 'Successfully shared wave'
            });
        })
        .catch(err => {
            console.log(err);
            return res.json(serverError);
        });
});

app.post('/api/posts/:postId/unshare', function(req, res, next) {
    if (!req.body.userId) {
        return res.json({
            status: 400,
            data: null,
            message: 'ID of user unsharing the post must be defined'
        });
    }

    let originalPostId = '';

    Post.findById(req.params.postId)
        .then(post => {
            if (post.isRepostOf) {
                //if this post is the repost, delete this post
                originalPostId = post.isRepostOf;
                return post;
            } else {
                // if this post is not the repost, find the repost and delete it
                originalPostId = req.params.postId;
                return Post.findOne({
                    userId: req.body.userId,
                    isRepostOf: req.params.postId
                });
            }
        })
        .then(post => {
            return Post.remove({_id: post._id});
        })
        .then(post => {
            return Post.findById(originalPostId);
        })
        .then(originalPost => {
            if (originalPost.numShares >0) { originalPost.numShares -= 1; }
            return originalPost.save();
        })
        .then(post => {
            return res.json({
                status: 200,
                data: post,
                message: 'Successfully unshared wave'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });

});

/*
 * Create new comment
 */
app.post('/api/comments', function(req, res, next) {
    const requiredParams = [
        {key: 'userId', readable: 'User'},
        {key: 'postId', readable: 'Post'},
        {key: 'body', readable: 'Comment body'}
    ];

    let missingParams = getMissingParams(req.body, requiredParams);
    if (missingParams.length > 0) {
        let missingParamStr = missingParams.join(', ');
        return res.json({
            status: 400,
            data: null,
            message: missingParamStr + ' is/are required.'
        });
    } else {
        var comment = new Comment();
        for (let param of requiredParams) {
            let key = param['key']
            comment[key] = req.body[key];
        }

        comment.save()
            .then(comment => {
                return Post.findById(req.body.postId);
            })
            .then(post => {
                post.comments.push(comment);
                sendNotification(req.body.userId,
                                 post.userId,
                                 ' commented on your wave.',
                                 { notifType: 2, id: post._id });
                return post.save();
            })
            .then(post => {
                return res.json({
                    status: 200,
                    data: comment,
                    message: 'Successfully added new comment.'
                });
            })
            .catch(err => {
                console.log(err);
                return res.json(serverError);
            });

    }

});

/*
 * Create new follower/following relationship
 */
app.post('/api/following', function(req, res, next) {
    let following = null;
    let isPrivate;
    User.findById(req.body.followeeId)
        .then(user => {
            isPrivate = user.isPrivate;
            if (user.isPrivate) {
                following = new FollowRequest({
                    followerId: req.body.followerId,
                    followeeId: req.body.followeeId
                });
            } else {
                following = new Following({
                    followerId: req.body.followerId,
                    followeeId: req.body.followeeId
                });
            }
            return following.save();
        })
        .then(following => {
            let message = (isPrivate) 
                            ? ' has requested to follow you.' 
                            : ' is now following you.';
            sendNotification(following['followerId'],
                             following['followeeId'],
                             message,
                             { notifType: 1, id: following['followerId'] });
            return res.json({
                status: 200,
                data: following,
                message: 'Success.' 
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
});

/*
 * Accept or decline follow request
 */
app.post('/api/followRequests/respond', function(req, res, next) {
    FollowRequest.remove({
        followerId: req.body.followerId,
        followeeId: req.body.followeeId
    })
    .then(request => {
        if (req.body.acceptedRequest) {
            let following = new Following({
                followerId: req.body.followerId,
                followeeId: req.body.followeeId
            });
            return following.save();
        } else {
            return null;
        }
    })
    .then(following => {
        if (req.body.acceptedRequest) {
            return res.json({
                status: 200,
                data: following,
                message: 'Accepted follow request.' 
            });
        } else {
            return res.json({
                status: 200,
                data: null,
                message: 'Declined follow request.' 
            });
        }
    })
    .catch(err => {
        return res.json(serverError);
    });
});

app.get('/api/followRequests/:userId', function(req, res, next) {
    FollowRequest.find({ followeeId: req.params.userId })
        .then(requests => {
            return res.json({
                status: 200,
                data: requests,
                message: 'Sucessfully retrieved follow requests.'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });
})


/*
 * Get all users followed by a particular user
 */
app.get('/api/users/:userId/following', function(req, res, next) {
    Following.find({followerId: req.params.userId})
        .then(following => {
            let followeeIds = [];
            for (let entry of following) {
                followeeIds.push(entry.followeeId);
            }
            return User.find({_id: {$in: followeeIds}});
        })
        .then(followees => {
            return res.json({
                status: 200,
                data: followees,
                message: 'Success.'
            })
        })
        .catch(err => {
            return res.json(serverError);
        });

});

/*
 * Get all users that are following a particular user
 */
app.get('/api/users/:userId/followers', function(req, res, next) {
    Following.find({followeeId: req.params.userId})
        .then(following => {
            let followerIds = [];
            for (let entry of following) {
                followerIds.push(entry.followerId);
            }
            return User.find({_id: {$in: followerIds}});
        })
        .then(followers => {
            return res.json({
                status: 200,
                data: followers,
                message: 'Success.'
            })
        })
        .catch(err => {
            return res.json(serverError);
        });
});

/*
 * Return 30 most recent waves made by users followed by this user
 */
app.get('/api/waves/:userId/:pageNum', function(req, res, next) {

    let thisPosts = [];
    let thisUsername = '';

    User.findById(req.params.userId)
        .then(user => {
            thisUsername = user.username;
            if (user) {
                return Following.find({ followerId: user['_id'] });
            } else {
                throw 'User was not found';
            }
        })
        .then(following => {
            if (following) {
                let ids = [];
                for (let entry of following) ids.push(entry['followeeId']);
                ids.push(req.params.userId); // include posts from this user.

                let pageNum = req.params.pageNum || 1;
                let pageSize = 10;
                let skipNum = pageSize * (pageNum-1);
                return Post.find({ userId: { $in: ids }})
                           .sort('-created')
                           .skip(skipNum)
                           .limit(pageSize);
            } else {
                // No posts by following because there are no users to follow
                return [];
            }
        })

        .then(posts => {
            thisPosts = posts;

        /* 
         * Mark posts that are already liked by this user
         */
            return UserLike.find({ userId: req.params.userId });
        })
        .then(userLikes => {
            let likedPostIds = {};
            for (let entry of userLikes) {
                likedPostIds[entry.postId] = true;
            }
            thisPosts.map(post => {
                post.likedByMe = (likedPostIds[post._id]) ? true : false;
                return post;
            });
            return thisPosts;
        })

        /* 
         * Mark posts that are already shared by this user
         */
        .then(posts => {
            thisPosts.map(post => {
                post.isRepostByMe = (post.repostUserData.username === thisUsername) ? true : false;
            });
            return res.json({
                status: 200,
                data: thisPosts,
                message: 'Recent waves by users you\'re following'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });

});

/* 
 * TEMPORARY TODO delete
 */
app.get('/api/waves/:userId', function(req, res, next) {

    let thisPosts = [];

    User.findById(req.params.userId)
        .then(user => {
            if (user) {
                return Following.find({ followerId: user['_id'] });
            } else {
                throw 'User was not found';
            }
        })
        .then(following => {
            if (following) {
                let ids = [];
                for (let entry of following) ids.push(entry['followeeId']);
                ids.push(req.params.userId); // include posts from this user.
                return Post.find({ userId: { $in: ids }})
                           .sort('-created')
                           .limit(10);
            } else {
                // No posts by following because there are no users to follow
                return [];
            }
        })

        .then(posts => {
            thisPosts = posts;

        /* 
         * Mark posts that are already liked by this user
         */
            return UserLike.find({ userId: req.params.userId });
        })
        .then(userLikes => {
            let likedPostIds = {};
            for (let entry of userLikes) {
                likedPostIds[entry.postId] = true;
            }
            thisPosts.map(post => {
                post.likedByMe = (likedPostIds[post._id]) ? true : false;
                return post;
            });

            return res.json({
                status: 200,
                data: thisPosts,
                message: 'Recent waves by users you\'re following'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });

});


/*
 * Delete existing follower/following relationship
 */
app.delete('/api/following', function(req, res, next) {
    const requiredParams = ['followeeId', 'followerId'];
    for (let param of requiredParams) {
        if (!req.body[param]) {
            return res.status(400).send('Missing parameter ' + param );
        }
    }

    const query = { 
        followeeId: req.body['followeeId'],
        followerId: req.body['followerId']
    };

    Following.findOneAndRemove(query, function(err, following) {
        if (err) { 
            return res.json({
                status: 500,
                data: null,
                message: 'There was an error. Please try again later.'
            });
        }
        return res.json({
            status: 200,
            data: following,
            message: 'Successfully removed record id ' + following['_id']
        });
    });
});

app.get('/api/users/search/:searchTerm', function(req, res, next) {
    let regexp = new RegExp(req.params.searchTerm, 'i');
    User.find({ $or: [{fullname: regexp }, {username: regexp} ] })
        .then(users => {
            return res.json({
                status: 200,
                data: users,
                message: 'Users matching search query'
            });
        })
        .catch(err => {
            return res.json(serverError);
        });

});

function getMissingParams(requestBody, requiredParams) {
    let missingParams = [];
    for (let param of requiredParams) {
        let key = param['key'];
        if (!requestBody[key]) missingParams.push(param['readable']);
    }
    return missingParams;
}

function sendNotification(senderId, recipientId, message, payload) {
    // User can't send notifications to themselves
    if (senderId === recipientId) {
        return;
    }
    let sender = {};
    let recipient = {};
    User.findById(senderId)
        .then(user => {
            sender = user;
            payload.facebookPicUrl = sender.facebookPicUrl;
            return User.findById(recipientId);
        })
        .then(user => {
            recipient = user;

            // Dispatch notification.
            let note = new apn.Notification();
            let deviceToken = recipient['deviceToken'];

            note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
            note.badge = 3;
            note.sound = "ping.aiff";
            note.alert = sender['fullname'] + message;
            note.payload = {};
            note.topic = "com.waves";
            note.payload = payload;

            return apnProvider.send(note, deviceToken);
        })
        .then(result => {
            if (result.sent.length > 0) {
                // create notif object
                let notif = new Notification();
                notif.body = sender['fullname'] + ' ' + message;
                notif.payload = {
                    notifType: payload.notifType,
                    id: payload.id
                };
                return notif.save();
            } else {
                throw result.failed;
            }
        })
        .then(notif => {
            // push to user object
            let allNotifs = recipient.allNotifications;
            let tagNotifs = recipient.tagNotifications;
            allNotifs.push(notif);
            if (payload.isTag) {
                tagNotifs.push(notif);
            }

            if (allNotifs.length > 20) allNotifs.shift();
            if (tagNotifs.length > 20) tagNotifs.shift();

            recipient.allNotifications = allNotifs;
            if (payload.isTag) {
                recipient.tagNotifications = tagNotifs;
            }
            return recipient.save();
        })
        .catch(err => {
            console.log(err);
        });
    
}

app.listen(8080);
console.log('Listening on port 8080');
