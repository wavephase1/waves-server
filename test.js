/*
 * Get a list of all users
 */
app.get('/api/users', function(req, res, next) {
    User.find({}, function(err, users) {
        if (err) {
            return res.json({
                status: 500,
                data: null,
                message: 'There was an error. Please try again later.' 
            });
        }
        return res.json({
            status: 200,
            data: users,
            message: 'Success'
        });
    });
});
